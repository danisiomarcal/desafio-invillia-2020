import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './navegacao/home/home.component';
import { AcessoNegadoComponent } from './navegacao/acesso-negado/acesso-negado.component';
import { NaoEncontradoComponent } from './navegacao/nao-encontrado/nao-encontrado.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'usuarios',
    loadChildren: () => import('./usuario/usuario.module').then(m => m.UsuarioModule)
  },
  {
    path: 'amigos',
    loadChildren: () => import('./amigo/amigo.module').then(m => m.AmigoModule)
  },
  {
    path: 'jogos',
    loadChildren: () => import('./jogo/jogo.module').then(m => m.JogoModule)
  },
  { path: 'acesso-negado', component: AcessoNegadoComponent },
  { path: '**', component: NaoEncontradoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
