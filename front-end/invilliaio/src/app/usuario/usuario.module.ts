import { HttpClient } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { UsuarioAppComponent } from './usuario-app.component';
import { LoginComponent } from './login/login.component';

import { CadastroComponent } from './cadastro/cadastro.component';
import { ListaComponent } from './lista/lista.component';

import { UsuarioRoutingModule } from './usuario-routing.module';
import { ExcluirComponent } from './excluir/excluir.component';
import { ConfirmarComponent } from './excluir/confirmar.component';

import { UsuarioGuard } from './services/usuario.guard';
import { UsuarioResolver } from './services/usuario.resolve';
import { UsuarioService } from './services/usuario.service';

@NgModule({
  declarations: [
    UsuarioAppComponent,
    LoginComponent,
    CadastroComponent,
    ListaComponent,
    ExcluirComponent,
    ConfirmarComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    UsuarioRoutingModule
  ],
  providers:[
    UsuarioGuard,
    UsuarioResolver,
    UsuarioService
  ]
})
export class UsuarioModule { }
