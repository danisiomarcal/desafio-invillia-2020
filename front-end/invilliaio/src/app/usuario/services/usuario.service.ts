import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";

import { BaseService } from 'src/app/services/base.service';
import { Usuario } from '../models/Usuario';

@Injectable()
export class UsuarioService extends BaseService {

  constructor(private http: HttpClient) { super(); }

  login(usuario: Usuario): Observable<any> {
    let response = this.http
      .post(this.urlServiceV1 + 'usuarios/autenticar', usuario, this.obterHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }

  cadastrar(usuario: Usuario): Observable<Usuario> {
    let response = this.http
        .post(this.urlServiceV1 + 'usuarios/novo', usuario, this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  alterar(usuario: Usuario): Observable<Usuario> {
    let response = this.http
        .put(this.urlServiceV1 + 'usuarios/' + usuario.id + '/alterar', usuario, this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  excluir(usuarioId: string): Observable<Usuario> {
    let response = this.http
        .delete(this.urlServiceV1 + 'usuarios/' + usuarioId + '/excluir', this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  obterPorId(id: string): Observable<Usuario> {
    let response = this.http
        .get(this.urlServiceV1 + 'usuarios/' + id, this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  obterTodos(): Observable<Usuario[]> {
    let response = this.http
        .get(this.urlServiceV1 + 'usuarios/listar', this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }
}
