import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { UsuarioService } from './usuario.service';
import { Usuario } from '../models/Usuario';

@Injectable()
export class UsuarioResolver implements Resolve<Usuario> {
    constructor(private service: UsuarioService) { }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.paramMap.has('id'))
            return this.service.obterPorId(route.paramMap.get('id'));

        return {
            id: null,
            nome: '',
            login: '',
            tipo: '',
            senha: '',
            ativo: true
        } as Usuario
    }
}