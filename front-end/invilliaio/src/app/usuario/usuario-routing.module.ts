import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsuarioAppComponent } from './usuario-app.component';
import { LoginComponent } from './login/login.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ListaComponent } from './lista/lista.component';

import { ExcluirComponent } from './excluir/excluir.component';

import { UsuarioGuard } from './services/usuario.guard';
import { UsuarioResolver } from './services/usuario.resolve';

const routes: Routes = [
  {
    path: '',
    component: UsuarioAppComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'cadastro',
        component: CadastroComponent,
        canActivate:[UsuarioGuard],
        data: [{ claim: { nome: 'Usuario', valor: 'Gerenciar' } }],
        resolve: { usuario: UsuarioResolver }
      },
      {
        path: 'cadastro/:id',
        component: CadastroComponent,
        canActivate:[UsuarioGuard],
        data: [{ claim: { nome: 'Usuario', valor: 'Gerenciar' } }],
        resolve: { usuario: UsuarioResolver }
      },
      {
        path: 'lista',
        component: ListaComponent,
        canActivate:[UsuarioGuard],
        data: [{ claim: { nome: 'Usuario', valor: 'Gerenciar' } }]
      },
      {
        path: 'excluir/:id',
        component: ExcluirComponent,
        canActivate:[UsuarioGuard],
        data: [{ claim: { nome: 'Usuario', valor: 'Gerenciar' } }],
        resolve: { usuario: UsuarioResolver }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UsuarioRoutingModule { }
