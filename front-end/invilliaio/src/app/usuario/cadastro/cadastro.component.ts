import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { CustomValidators } from 'ngx-custom-validators';

import { FormBaseComponent } from 'src/app/components/form-base.component';
import { Usuario } from '../models/Usuario';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html'
})
export class CadastroComponent extends FormBaseComponent implements OnInit {
  erros: string[] = [];
  private usuario: Usuario;

  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private usuarioService: UsuarioService) {

    super();

    this.formMensagensValidacao = {
      nome: { required: 'Nome obrigatório.' },
      login: { required: 'Login obrigatório.' },
      senha: { required: 'Senha obrigatória.' },
      senhaConfirmacao: { required: 'Confirmação obrigatória.', equalTo: 'As senhas não conferem.' },
      tipo: { required: 'Tipo obrigatório.' }
    }

    this.usuario = this.route.snapshot.data['usuario'];
  }

  ngOnInit(): void {

    let senha = new FormControl(this.usuario.senha, [Validators.required]);
    let senhaConfirmacao = new FormControl(this.usuario.senha, [Validators.required, CustomValidators.equalTo(senha)]);

    this.formulario = this.fb.group({
      nome: [this.usuario.nome, Validators.required],
      login: [this.usuario.login, Validators.required],
      senha: senha,
      senhaConfirmacao: senhaConfirmacao,
      tipo: [this.usuario.tipo, Validators.required],
      ativo: [this.usuario.ativo]
    });
  }

  salvar() {

    this.validarFormulario();
  
    if (this.formulario.valid) {

      this.usuario = Object.assign({}, this.usuario, this.formulario.value);

      if (this.usuario.id == null) {
        this.usuarioService.cadastrar(this.usuario)
          .subscribe(
            sucesso => { this.processarSucesso(sucesso) },
            falha => { this.processarFalha(falha) }
          );
      } else {
        this.usuarioService.alterar(this.usuario)
          .subscribe(
            sucesso => { this.processarSucesso(sucesso) },
            falha => { this.processarFalha(falha) }
          );
      }
    }
  }

  processarSucesso(response: any) {

    let toast = this.toastr.success('Usuário salvo com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/usuarios/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
