import { JwtUtils } from 'src/app/utils/JwtUtils';
import { LocalStorageUtils } from 'src/app/utils/LocalStoreUtils';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Usuario } from '../models/Usuario';
import { ConfirmarComponent } from './confirmar.component';
import { UsuarioService } from './../services/usuario.service';

@Component({
  selector: 'app-excluir',
  templateUrl: './excluir.component.html'
})
export class ExcluirComponent implements OnInit {

  erros: string[] = [];
  podeExcluir: boolean = true;
  usuario: Usuario;

  localStorageUtils: LocalStorageUtils = new LocalStorageUtils();
  jwtUtils: JwtUtils = new JwtUtils();

  constructor(private modalService: NgbModal,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private usuarioService: UsuarioService) {

    this.usuario = this.route.snapshot.data['usuario'];
  }

  ngOnInit(): void {
    let idUsuario = this.jwtUtils.extrairClaimSubToken(this.localStorageUtils.obterTokenUsuario());

    if (idUsuario === this.usuario.id) {
      this.erros = ["Não é possível excluir o usuário logado."];
      this.podeExcluir = false;
    }
  }

  excluir() {
    const modalRef = this.modalService.open(ConfirmarComponent);

    modalRef.componentInstance.usuario = this.usuario;

    modalRef.componentInstance.exclusaoConfimadaEmitter.subscribe(() => {
      this.usuarioService.excluir(this.usuario.id)
        .subscribe(
          sucesso => { this.processarSucesso(sucesso) },
          falha => { this.processarFalha(falha) }
        );
    });
  }

  processarSucesso(response: any) {
    let toast = this.toastr.success('Usuário excluído com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/usuarios/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
