import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-confirmar',
  templateUrl: './confirmar.component.html'
})
export class ConfirmarComponent implements OnInit {

  @Input() usuario;

  @Output() exclusaoConfimadaEmitter = new EventEmitter();

  constructor(public modal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  confirmar() {
    this.exclusaoConfimadaEmitter.emit();
    this.modal.dismiss();
  }
}
