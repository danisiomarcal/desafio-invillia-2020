import { JsonPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { FormBaseComponent } from 'src/app/components/form-base.component';

import { LocalStorageUtils } from 'src/app/utils/LocalStoreUtils';
import { Usuario } from '../models/Usuario';

import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent extends FormBaseComponent implements OnInit {
  erros: string[] = [];
  localStorageUtils: LocalStorageUtils = new LocalStorageUtils();

  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private usuarioService: UsuarioService) {

    super();
      
    this.formMensagensValidacoesCampos = {
      login: { required: 'Login obrigatório.' },
      senha: { required: 'Senha obrigatória.' }
    }
  }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      login: ['', Validators.required],
      senha: ['', Validators.required],
      frameworks: this.buildFrameworks()
    });
  }

  valores = [{ id: 1, nome: 'danisio' }, { id: 2, nome: 'alana' }, { id: 3, nome: 'monique' }];

  buildFrameworks() {
    return this.fb.array(this.valores.map(v => new FormControl(false)));
  }

  logar() {

    this.validarFormulario();

    if (this.formulario.valid) {

      let usuario = {} as Usuario;

      let form = Object.assign({}, this.formulario.value);
      
      form = Object.assign(form, {
        frameworks: form.frameworks
        .map((v, i) => v ? this.valores[i] : null)
        .filter(v => v !== null)
      });

      console.log(form);

      this.usuarioService.login(usuario)
        .subscribe(
          sucesso => { this.processarSucesso(sucesso) },
          falha => { this.processarFalha(falha) }
        );
    }
  }

  processarSucesso(response: any) {

    this.localStorageUtils.salvarDadosLocaisUsuario(response);

    let toast = this.toastr.success('Login realizado com sucesso!', null, { timeOut: 1000 });

    toast.onHidden.subscribe(() => {

      this.router.navigate(['/home']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
