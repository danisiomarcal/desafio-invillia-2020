export interface Usuario {
    id: string,
    nome: string,
    login: string,
    tipo: string,
    senha: string,
    ativo: boolean
}