import { FormGroup } from '@angular/forms';

export abstract class FormBaseComponent {

  formulario: FormGroup;

  protected formMensagensValidacoesCampos: MensagemValidacaoCampo;

  public getErrosCampo(campo: string): string[] {

    let erros: string[] = [];

    const controle = this.formulario.get(campo);

    if (controle.invalid && (controle.touched || controle.dirty)) {

      Object.keys(controle.errors).forEach(erro => {
        erros.push(this.formMensagensValidacoesCampos[campo][erro]);
      });
    }

    return erros;
  }

  public validarFormulario() {
    if (this.formulario.invalid)
      this.validarFormGroup(this.formulario);
  }

  private validarFormGroup(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(campo => {

      const controle = formGroup.get(campo);

      if (controle.invalid)
        controle.markAsDirty();

      if (controle instanceof FormGroup)
        this.validarFormGroup(controle);
    });
  }
}

export interface MensagemValidacaoCampo {
  [key: string]: { [key: string]: string }
}