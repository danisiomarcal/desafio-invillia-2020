import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { AcessoNegadoComponent } from './acesso-negado/acesso-negado.component';
import { NaoEncontradoComponent } from './nao-encontrado/nao-encontrado.component';

@NgModule({
  declarations: [
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    AcessoNegadoComponent,
    NaoEncontradoComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule
  ],
  exports:[
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    AcessoNegadoComponent,
    NaoEncontradoComponent
  ]
})
export class NavegacaoModule { }
