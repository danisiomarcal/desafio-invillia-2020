import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LocalStorageUtils } from 'src/app/utils/LocalStoreUtils';
import { JwtUtils } from 'src/app/utils/JwtUtils';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

  private localStorageUtils: LocalStorageUtils = new LocalStorageUtils();
  private jwtUtils: JwtUtils = new JwtUtils();

  constructor(private router: Router) { }

  title: string = "Desafio Invillia - 2020";

  ngOnInit(): void {
  }

  public get usuarioAutenticado(): boolean {
    return this.jwtUtils.tokenEhValido(this.localStorageUtils.obterTokenUsuario());
  }

  public get nomeUsuario(): string {
    return this.localStorageUtils.obterUsuario().nome;
  }

  logout() {
    this.localStorageUtils.limparDadosLocaisUsuario();

    this.router.navigate(['/']);
  }
}
