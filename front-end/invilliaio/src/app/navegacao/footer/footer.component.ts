import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

  constructor() { }

  public title: string = "Defasio Invillia - 2020";

  ngOnInit(): void {
  }

}
