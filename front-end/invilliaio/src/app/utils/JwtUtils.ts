import { JwtHelperService } from "@auth0/angular-jwt";

export class JwtUtils {

    public extrairClaimSubToken(token: string) {
        if (!token || token == null)
            return null;

        const jwtHelper = new JwtHelperService();

        const tokenDecode = jwtHelper.decodeToken(token);

        return tokenDecode.sub;
    }

    public tokenEhValido(token: string): boolean {
        if (!token || token == null)
            return false;

        const jwtHelper = new JwtHelperService();

        const tokenDecode = jwtHelper.decodeToken(token);

        if (tokenDecode.exp === undefined) {
            return false;
        }

        const dataCorrente = new Date(0);

        let tokenExpData = dataCorrente.setUTCSeconds(tokenDecode.exp);

        return tokenExpData.valueOf() > new Date().valueOf();
    }
}