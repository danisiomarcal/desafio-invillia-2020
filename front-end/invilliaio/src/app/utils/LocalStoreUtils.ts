export class LocalStorageUtils {
    
    public obterUsuario() {
        return JSON.parse(localStorage.getItem('invillia.user'));
    }

    public salvarDadosLocaisUsuario(response: any) {
        this.salvarTokenUsuario(response.token);
        this.salvarUsuario(response.userToken);
    }

    public limparDadosLocaisUsuario() {
        localStorage.removeItem('invillia.token');
        localStorage.removeItem('invillia.user');
    }

    public obterTokenUsuario(): string {
        return localStorage.getItem('invillia.token');
    }

    private salvarTokenUsuario(token: string) {
        localStorage.setItem('invillia.token', token);
    }

    private salvarUsuario(user: any) {
        localStorage.setItem('invillia.user', JSON.stringify(user));
    }
}