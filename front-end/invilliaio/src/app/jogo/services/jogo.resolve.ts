import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { Jogo } from './../models/jogo';
import { JogoService } from './jogo.service';

@Injectable()
export class JogoResolver implements Resolve<Jogo> {
    constructor(private service: JogoService) { }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.paramMap.has('id'))
            return this.service.obterPorId(route.paramMap.get('id'));

        return {
            id: null,
            nome: ''
        } as Jogo
    }
}