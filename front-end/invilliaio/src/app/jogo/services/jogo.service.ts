import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";

import { BaseService } from 'src/app/services/base.service';
import { Jogo } from '../models/jogo';

@Injectable()
export class JogoService extends BaseService {

  constructor(private http: HttpClient) { super(); }

  cadastrar(jogo: Jogo): Observable<Jogo> {
    let response = this.http
      .post(this.urlServiceV1 + 'jogos/novo', jogo, this.obterAuthHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }

  alterar(jogo: Jogo): Observable<Jogo> {
    let response = this.http
      .put(this.urlServiceV1 + 'jogos/' + jogo.id + '/alterar', jogo, this.obterAuthHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }

  excluir(jogoId: string): Observable<Jogo> {
    let response = this.http
      .delete(this.urlServiceV1 + 'jogos/' + jogoId + '/excluir', this.obterAuthHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }

  obterPorId(id: string): Observable<Jogo> {
    let response = this.http
      .get(this.urlServiceV1 + 'jogos/' + id, this.obterAuthHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }

  obterTodos(): Observable<Jogo[]> {
    let response = this.http
      .get(this.urlServiceV1 + 'jogos/listar', this.obterAuthHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }

  registrarEmprestimo(jogoId: string, amigoId: string): Observable<Jogo> {
    let response = this.http
      .post(this.urlServiceV1 + 'jogos/' + jogoId + '/registrar-emprestimo', { AmigoId: amigoId }, this.obterAuthHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }

  registrarDevolucao(jogoId: string): Observable<Jogo> {
    let response = this.http
      .put(this.urlServiceV1 + 'jogos/' + jogoId + '/registrar-devolucao', {}, this.obterAuthHeaderJson())
      .pipe(
        map(this.extractData),
        catchError(this.serviceError));

    return response;
  }
}