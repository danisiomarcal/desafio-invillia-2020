import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBaseComponent } from 'src/app/components/form-base.component';
import { Jogo } from '../models/jogo';
import { JogoService } from '../services/jogo.service';
import { AmigoService } from 'src/app/amigo/services/amigo.service';
import { Amigo } from 'src/app/amigo/models/Amigo';

@Component({
  selector: 'app-emprestar',
  templateUrl: './emprestar.component.html'
})
export class EmprestarComponent extends FormBaseComponent implements OnInit {

  erros: string[] = [];

  amigos: Amigo[] = [];
  jogo: Jogo;

  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private jogoService: JogoService,
    private amigoService: AmigoService) {

    super();

    this.formMensagensValidacao = {
      amigo: { required: 'Escolha um amigo!' }
    }

    this.jogo = this.route.snapshot.data['jogo'];
  }

  ngOnInit(): void {
    this.formulario = this.fb.group({
      amigo: ['', Validators.required]
    });

    this.amigoService.obterTodos()
      .subscribe(amigos => this.amigos = amigos);
  }

  emprestar() {
    this.validarFormulario();

    if (this.formulario.valid) {
      let amigoId = this.formulario.get('amigo').value;

      this.jogoService.registrarEmprestimo(this.jogo.id, amigoId)
        .subscribe(
          sucesso => { this.processarSucesso(sucesso) },
          falha => { this.processarFalha(falha) }
        );
    }
  }

  processarSucesso(response: any) {

    let toast = this.toastr.success('Jogo emprestado com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/jogos/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
