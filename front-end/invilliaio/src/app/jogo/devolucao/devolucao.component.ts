import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Jogo } from '../models/jogo';
import { JogoService } from '../services/jogo.service';

@Component({
  selector: 'app-devolucao',
  templateUrl: './devolucao.component.html'
})
export class DevolucaoComponent implements OnInit {

  erros: string[] = [];

  jogo: Jogo;

  constructor(private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private jogoService: JogoService) {

    this.jogo = this.route.snapshot.data['jogo'];
  }

  ngOnInit(): void {
  }

  devolver() {
    this.jogoService.registrarDevolucao(this.jogo.id)
      .subscribe(
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
      );
  }

  processarSucesso(response: any) {

    let toast = this.toastr.success('Jogo devolvido com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/jogos/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
