import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Jogo } from '../models/jogo';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styles: [
  ]
})
export class DetalhesComponent implements OnInit {

  jogo: Jogo;

  constructor(private route: ActivatedRoute) {
    this.jogo = this.route.snapshot.data['jogo'];
  }

  ngOnInit(): void {
  }
}
