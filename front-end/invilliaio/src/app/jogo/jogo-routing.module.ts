import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { JogoAppComponent } from './jogo-app.component';

import { CadastroComponent } from './cadastro/cadastro.component';
import { EmprestarComponent } from './emprestar/emprestar.component';
import { ExcluirComponent } from './excluir/excluir.component';
import { ListaComponent } from './lista/lista.component';
import { DevolucaoComponent } from './devolucao/devolucao.component';

import { JogoGuard } from './services/jogo.guard';
import { JogoResolver } from './services/jogo.resolve';
import { DetalhesComponent } from './detalhes/detalhes.component';

const routes: Routes = [
  {
    path: '',
    component: JogoAppComponent,
    canActivate: [JogoGuard],
    data: [{ claim: { nome: 'Jogo', valor: 'Gerenciar' } }],
    children: [
      {
        path: 'cadastro',
        component: CadastroComponent,
        resolve: { jogo: JogoResolver }
      },
      {
        path: 'cadastro/:id',
        component: CadastroComponent,
        resolve: { jogo: JogoResolver }
      },
      {
        path: 'detalhes/:id',
        component: DetalhesComponent,
        resolve: { jogo: JogoResolver }
      },
      {
        path: 'lista',
        component: ListaComponent
      },
      {
        path: 'emprestar/:id',
        component: EmprestarComponent,
        resolve: { jogo: JogoResolver }
      },
      {
        path: 'devolucao/:id',
        component: DevolucaoComponent,
        resolve: { jogo: JogoResolver }
      },
      {
        path: 'excluir/:id',
        component: ExcluirComponent,
        resolve: { jogo: JogoResolver }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class JogoRoutingModule { }
