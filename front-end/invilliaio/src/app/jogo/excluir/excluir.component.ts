import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JogoService } from './../services/jogo.service';
import { Jogo } from '../models/jogo';

@Component({
  selector: 'app-excluir',
  templateUrl: './excluir.component.html'
})
export class ExcluirComponent implements OnInit {

  jogo: Jogo;
  erros: string[] = [];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private jogoService: JogoService) {

    this.jogo = this.route.snapshot.data['jogo'];
  }


  ngOnInit(): void {
  }

  excluir() {
    this.jogoService.excluir(this.jogo.id)
      .subscribe(
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
      );
  }

  processarSucesso(response: any) {
    let toast = this.toastr.success('Jogo excluído com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/jogos/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
