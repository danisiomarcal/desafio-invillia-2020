import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { JogoRoutingModule } from './jogo-routing.module';

import { JogoAppComponent } from './jogo-app.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { EmprestarComponent } from './emprestar/emprestar.component';
import { ExcluirComponent } from './excluir/excluir.component';
import { ListaComponent } from './lista/lista.component';
import { DevolucaoComponent } from './devolucao/devolucao.component';

import { JogoGuard } from './services/jogo.guard';
import { JogoResolver } from './services/jogo.resolve';
import { JogoService } from './services/jogo.service';
import { DetalhesComponent } from './detalhes/detalhes.component';
import { AmigoModule } from '../amigo/amigo.module';

@NgModule({
  declarations: [
    JogoAppComponent,
    CadastroComponent,
    EmprestarComponent,
    ExcluirComponent,
    ListaComponent,
    DevolucaoComponent,
    DetalhesComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    JogoRoutingModule,
    AmigoModule
  ],
  providers:[
    JogoGuard,
    JogoService,
    JogoResolver
  ]
})
export class JogoModule { }
