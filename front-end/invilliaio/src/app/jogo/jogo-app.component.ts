import { Component } from '@angular/core';

@Component({
  selector: 'app-jogo',
  template: '<router-outlet></router-outlet>'
})
export class JogoAppComponent {
}
