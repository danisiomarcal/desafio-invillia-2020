import { Component, OnInit } from '@angular/core';
import { Jogo } from '../models/jogo';
import { JogoService } from '../services/jogo.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html'
})
export class ListaComponent implements OnInit {

  jogos: Jogo[] = [];

  constructor(private jogoService: JogoService) { }

  ngOnInit(): void {
    this.jogoService.obterTodos()
      .subscribe(
        jogos => { this.jogos = jogos }
      );
  }
}
