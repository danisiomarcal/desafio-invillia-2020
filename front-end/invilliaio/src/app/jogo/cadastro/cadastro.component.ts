import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { FormBaseComponent } from 'src/app/components/form-base.component';
import { Jogo } from '../models/jogo';
import { JogoService } from '../services/jogo.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html'
})
export class CadastroComponent extends FormBaseComponent implements OnInit {

  erros: string[] = [];
  private jogo: Jogo;

  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private jogoService: JogoService) {
    super();

    this.formMensagensValidacao = {
      nome: { required: 'Nome obrigatório.' }
    }

    this.jogo = this.route.snapshot.data['jogo'];
  }

  ngOnInit(): void {

    this.formulario = this.fb.group({
      nome: [this.jogo.nome, Validators.required]
    });
  }

  salvar() {

    this.validarFormulario();

    if (this.formulario.valid) {
      this.jogo = Object.assign({}, this.jogo, this.formulario.value);

      if (this.jogo.id == null) {
        this.jogoService.cadastrar(this.jogo)
          .subscribe(
            sucesso => { this.processarSucesso(sucesso) },
            falha => { this.processarFalha(falha) }
          );
      } else {
        this.jogoService.alterar(this.jogo)
          .subscribe(
            sucesso => { this.processarSucesso(sucesso) },
            falha => { this.processarFalha(falha) }
          );
      }
    }
  }

  processarSucesso(response: any) {

    let toast = this.toastr.success('Jogo salvo com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/jogos/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}