import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Amigo } from '../models/Amigo';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styles: [
  ]
})
export class DetalhesComponent implements OnInit {

  public amigo: Amigo;

  constructor(private route: ActivatedRoute) {
    this.amigo = this.route.snapshot.data['amigo'];
  }

  ngOnInit(): void {
  }

  temJogoEmprestado(): boolean {
    return this.amigo.jogosEmprestados !== null && this.amigo.jogosEmprestados.length > 0;
  }
}
