import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';

import { Amigo } from '../models/Amigo';
import { AmigoService } from './amigo.service';

@Injectable()
export class AmigoResolver implements Resolve<Amigo> {
    constructor(private service: AmigoService) { }

    resolve(route: ActivatedRouteSnapshot) {
        if (route.paramMap.has('id'))
            return this.service.obterPorId(route.paramMap.get('id'));

        return {
            id: null,
            nome: ''
        } as Amigo
    }
}