import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";

import { BaseService } from 'src/app/services/base.service';
import { Amigo } from '../models/Amigo';

@Injectable()
export class AmigoService extends BaseService {

  constructor(private http: HttpClient) { super(); }

  cadastrar(amigo: Amigo): Observable<Amigo> {
    let response = this.http
        .post(this.urlServiceV1 + 'amigos/novo', amigo, this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  alterar(amigo: Amigo): Observable<Amigo> {
    let response = this.http
        .put(this.urlServiceV1 + 'amigos/' + amigo.id + '/alterar', amigo, this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  excluir(amigoId: string): Observable<Amigo> {
    let response = this.http
        .delete(this.urlServiceV1 + 'amigos/' + amigoId + '/excluir', this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  obterPorId(id: string): Observable<Amigo> {
    let response = this.http
        .get(this.urlServiceV1 + 'amigos/' + id, this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }

  obterTodos(): Observable<Amigo[]> {
    let response = this.http
        .get(this.urlServiceV1 + 'amigos/listar', this.obterAuthHeaderJson())
        .pipe(
            map(this.extractData),
            catchError(this.serviceError));

    return response;
  }
}