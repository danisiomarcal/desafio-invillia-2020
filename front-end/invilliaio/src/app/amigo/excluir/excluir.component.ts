import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Amigo } from '../models/Amigo';
import { AmigoService } from './../services/amigo.service';

@Component({
  selector: 'app-excluir',
  templateUrl: './excluir.component.html'
})
export class ExcluirComponent implements OnInit {

  amigo: Amigo;
  erros: string[] = [];

  constructor(private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private amigoService: AmigoService) {

    this.amigo = this.route.snapshot.data['amigo'];
  }


  ngOnInit(): void {
  }

  excluir() {
    this.amigoService.excluir(this.amigo.id)
      .subscribe(
        sucesso => { this.processarSucesso(sucesso) },
        falha => { this.processarFalha(falha) }
      );
  }

  processarSucesso(response: any) {
    let toast = this.toastr.success('Amigo excluído com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/amigos/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
