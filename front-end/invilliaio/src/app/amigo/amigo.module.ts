import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AmigoRoutingModule } from './amigo-routing.module';

import { AmigoAppComponent } from './amigo-app.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ListaComponent } from './lista/lista.component';
import { ExcluirComponent } from './excluir/excluir.component';
import { DetalhesComponent } from './detalhes/detalhes.component';

import { AmigoGuard } from './services/amigo.guard';
import { AmigoResolver } from './services/amigo.resolve';
import { AmigoService } from './services/amigo.service';

@NgModule({
  declarations: [
    AmigoAppComponent,
    CadastroComponent,
    ListaComponent,
    ExcluirComponent,
    DetalhesComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    AmigoRoutingModule
  ],
  providers:[
    AmigoGuard,
    AmigoService,
    AmigoResolver
  ]
})
export class AmigoModule { }
