import { Component } from '@angular/core';

@Component({
  selector: 'app-amigo',
  template: '<router-outlet></router-outlet>'
})
export class AmigoAppComponent {
}
