import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { FormBaseComponent } from 'src/app/components/form-base.component';
import { Amigo } from '../models/Amigo';
import { AmigoService } from '../services/amigo.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html'
})
export class CadastroComponent extends FormBaseComponent implements OnInit {

  erros: string[] = [];
  private amigo: Amigo;

  constructor(private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private amigoService: AmigoService) {
    super();

    this.formMensagensValidacao = {
      nome: { required: 'Nome obrigatório.' }
    }

    this.amigo = this.route.snapshot.data['amigo'];
  }

  ngOnInit(): void {

    this.formulario = this.fb.group({
      nome: [this.amigo.nome, Validators.required]
    });
  }

  salvar() {

    this.validarFormulario();

    if (this.formulario.valid) {
      this.amigo = Object.assign({}, this.amigo, this.formulario.value);

      if (this.amigo.id == null) {
        this.amigoService.cadastrar(this.amigo)
          .subscribe(
            sucesso => { this.processarSucesso(sucesso) },
            falha => { this.processarFalha(falha) }
          );
      } else {
        this.amigoService.alterar(this.amigo)
          .subscribe(
            sucesso => { this.processarSucesso(sucesso) },
            falha => { this.processarFalha(falha) }
          );
      }
    }
  }

  processarSucesso(response: any) {

    let toast = this.toastr.success('Amigo salvo com sucesso!', null, { timeOut: 1500 });

    toast.onHidden.subscribe(() => {
      this.router.navigate(['/amigos/lista']);
    });
  }

  processarFalha(fail: any) {
    this.erros = fail.error.data;
  }
}
