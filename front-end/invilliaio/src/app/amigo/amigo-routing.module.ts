import { AmigoResolver } from './services/amigo.resolve';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';

import { AmigoAppComponent } from './amigo-app.component';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ListaComponent } from './lista/lista.component';
import { ExcluirComponent } from './excluir/excluir.component';
import { DetalhesComponent } from './detalhes/detalhes.component';

import { AmigoGuard } from './services/amigo.guard';

const routes: Routes = [
  {
    path: '',
    component: AmigoAppComponent,
    canActivate: [AmigoGuard],
    data: [{ claim: { nome: 'Amigo', valor: 'Gerenciar' } }],
    children: [
      {
        path: 'cadastro',
        component: CadastroComponent,
        resolve: { amigo: AmigoResolver }
      },
      {
        path: 'cadastro/:id',
        component: CadastroComponent,
        resolve: { amigo: AmigoResolver }
      },
      {
        path: 'lista',
        component: ListaComponent
      },
      {
        path: 'detalhes/:id',
        component: DetalhesComponent,
        resolve: { amigo: AmigoResolver }
      },
      {
        path: 'excluir/:id',
        component: ExcluirComponent,
        resolve: { amigo: AmigoResolver }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AmigoRoutingModule { }
