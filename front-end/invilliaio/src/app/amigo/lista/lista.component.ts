import { Component, OnInit } from '@angular/core';
import { Amigo } from '../models/Amigo';
import { AmigoService } from '../services/amigo.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html'
})
export class ListaComponent implements OnInit {

  amigos: Amigo[] = [];

  constructor(private amigoService: AmigoService) { }

  ngOnInit(): void {
    this.amigoService.obterTodos()
      .subscribe(
        amigos => { this.amigos = amigos }
      );
  }
}
