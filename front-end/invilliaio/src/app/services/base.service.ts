import { HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { throwError } from "rxjs";
import { environment } from 'src/environments/environment';
import { LocalStorageUtils } from '../utils/LocalStoreUtils';

export abstract class BaseService {

    protected urlServiceV1: string = environment.apiUrlv1;
    public localStorage: LocalStorageUtils = new LocalStorageUtils();

    protected obterHeaderJson() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
    }

    protected obterAuthHeaderJson() {
        return {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.localStorage.obterTokenUsuario()}`
            })
        };
    }

    protected extractData(response: any) {
        return response.data || { };
    }

    protected serviceError(response: Response | any) {
        let customError: string[] = [];
        let customResponse = { error: { data: [] }}

        if (response instanceof HttpErrorResponse) {

            if (response.statusText === "Unknown Error") {
                customError.push("Ocorreu um erro desconhecido");
                customResponse.error.data = customError;
                return throwError(customResponse);
            }
        }

        if (response.status === 500) {
            customError.push("Ocorreu um erro no processamento, tente novamente mais tarde ou contate o nosso suporte."); 
            customResponse.error.data = customError;
            return throwError(customResponse);
        }

        console.error(response);
        return throwError(response);
    }
}