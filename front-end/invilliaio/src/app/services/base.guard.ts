import { ActivatedRouteSnapshot, Router } from '@angular/router';

import { LocalStorageUtils } from '../utils/LocalStoreUtils';
import { JwtUtils } from '../utils/JwtUtils';

export abstract class BaseGuard {

    private localStorageUtils: LocalStorageUtils = new LocalStorageUtils();

    private jwtUtils: JwtUtils = new JwtUtils();

    constructor(protected router: Router) { }

    protected validarClaims(routeAc: ActivatedRouteSnapshot): boolean {

        const token = this.localStorageUtils.obterTokenUsuario();

        if (!token || !this.jwtUtils.tokenEhValido(token)) {
            this.router.navigate(['/usuarios/login/']);
        }

        let usuario = this.localStorageUtils.obterUsuario();

        let claim: any = routeAc.data[0];

        if (claim !== undefined) {
            let claim = routeAc.data[0]['claim'];

            if (claim) {
                if (!usuario.claims) {
                    this.navegarAcessoNegado();
                }

                let userClaims = usuario.claims.find(x => x.tipo === claim.nome);

                if (!userClaims) {
                    this.navegarAcessoNegado();
                }

                let valoresClaim = userClaims.valor as string;

                if (!valoresClaim.includes(claim.valor)) {
                    this.navegarAcessoNegado();
                }
            }
        }

        return true;
    }

    private navegarAcessoNegado() {
        this.router.navigate(['/acesso-negado']);
    }
}