﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InvilliaIO.Application.ViewModels.Request
{
    public class EmprestimoJogoViewModel
    {
        /// <summary>
        /// Id do Amigo
        /// </summary>
        [Required(ErrorMessage = "O campo {0} é obrigatório.")]
        public Guid AmigoId { get; set; }
    }
}
