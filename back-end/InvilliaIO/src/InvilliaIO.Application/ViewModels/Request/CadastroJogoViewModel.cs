﻿using System.ComponentModel.DataAnnotations;

namespace InvilliaIO.Application.ViewModels.Request
{
    public class CadastroJogoViewModel
    {
        /// <summary>
        /// Nome do jogo
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        public string Nome { get; set; }
    }
}
