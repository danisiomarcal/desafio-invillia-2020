﻿using System.ComponentModel.DataAnnotations;

namespace InvilliaIO.Application.ViewModels.Request
{
    public class CadastroAmigoViewModel
    {
        /// <summary>
        /// Nome do amigo
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        public string Nome { get; set; }
    }
}
