﻿using System.ComponentModel.DataAnnotations;

namespace InvilliaIO.Application.ViewModels.Request
{
    public class CadastroUsuarioViewModel
    {
        /// <summary>
        /// Nome do usuário
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        public string Nome { get; set; }

        /// <summary>
        /// Login do usuário
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        [RegularExpression(@"^\S+$", ErrorMessage = "O campo {0} não pode conter espaços.")]
        public string Login { get; set; }

        /// <summary>
        /// Senha do usuário
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        [RegularExpression(@"^\S+$", ErrorMessage = "O campo {0} não pode conter espaços.")]
        public string Senha { get; set; }

        /// <summary>
        /// Usuário ativo, escolha 1 para verdadeiro ou 0 para falso
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        //[Range(0, 1, ErrorMessage = "Escolha 1 para que campo {0} seja verdadeiro e 0 para falso.")]
        public bool Ativo { get; set; }

        /// <summary>
        /// Tipo do usuário, escolha A(a) para Administrador ou N(n) para Normal
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        [RegularExpression(@"^([A]|[N])$", ErrorMessage = "Escolha A para que campo {0} seja Administrador e N para Normal.")]
        public string Tipo { get; set; }
    }
}
