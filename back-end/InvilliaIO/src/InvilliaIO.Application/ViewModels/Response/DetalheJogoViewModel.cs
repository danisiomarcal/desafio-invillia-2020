﻿using InvilliaIO.Domain.Modelos;
using System;

namespace InvilliaIO.Application.ViewModels.Response
{
    public class DetalheJogoViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string EmprestadoPara { get; set; }

        public DetalheJogoViewModel(Jogo jogo)
        {
            Id = jogo.Id;
            Nome = jogo.Nome;

            if (jogo.EstaEmprestado())
                EmprestadoPara = jogo.AmigoEmprestimo.Nome;
        }
    }
}
