﻿using InvilliaIO.Domain.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InvilliaIO.Application.ViewModels.Response
{
    public class DetalheAmigoViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public List<string> JogosEmprestados { get; set; }

        public DetalheAmigoViewModel(Amigo amigo)
        {
            Id = amigo.Id;
            Nome = amigo.Nome;

            if (amigo.TemJogoEmprestado())
                JogosEmprestados = amigo.JogosEmprestados.Select(j => j.Nome).ToList();
        }
    }
}
