﻿using System;
using InvilliaIO.Domain;

namespace InvilliaIO.Application.ViewModels.Response
{
    public class DetalheUsuarioViewModel
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }
        public string Tipo { get; set; }
        public bool Ativo { get; set; }

        public DetalheUsuarioViewModel(Usuario usuario)
        {
            Id = usuario.Id;
            Nome = usuario.Nome;
            Login = usuario.Login;
            Senha = usuario.Senha;
            Tipo = ((char)usuario.Tipo).ToString();
            Ativo = usuario.Ativo;
        }
    }
}
