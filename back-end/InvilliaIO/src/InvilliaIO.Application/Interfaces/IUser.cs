﻿using System;

namespace InvilliaIO.Application.Interfaces
{
    public interface IUser
    {
        string Nome { get; }
        Guid GetId();
        bool EstaAutenticado();
    }
}
