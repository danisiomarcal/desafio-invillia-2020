﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InvilliaIO.Application.ViewModels.Response;

namespace InvilliaIO.Application.Queries
{
    public interface IUsuarioQueries
    {
        Task<IEnumerable<DetalheUsuarioViewModel>> ObterTodosUsuarios();
        Task<IEnumerable<DetalheAmigoViewModel>> ObterTodosAmigos(Guid usuarioId);
        Task<IEnumerable<DetalheJogoViewModel>> ObterTodosJogos(Guid usuarioId);
    }
}
