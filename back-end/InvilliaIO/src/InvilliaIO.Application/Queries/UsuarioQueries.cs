﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InvilliaIO.Application.ViewModels.Response;
using InvilliaIO.Domain.Interfaces;

namespace InvilliaIO.Application.Queries
{
    public class UsuarioQueries : IUsuarioQueries
    {
        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioQueries(IUsuarioRepository usuarioRepository)
        {
            _usuarioRepository = usuarioRepository;
        }

        public async Task<IEnumerable<DetalheUsuarioViewModel>> ObterTodosUsuarios()
        {
            List<DetalheUsuarioViewModel> detalheUsuariosViewModel = new List<DetalheUsuarioViewModel>();

            var usuarios = await _usuarioRepository.ObterTodos();

            return usuarios.Select(usuario => new DetalheUsuarioViewModel(usuario)).ToList();
        }

        public async Task<IEnumerable<DetalheAmigoViewModel>> ObterTodosAmigos(Guid usuarioId)
        {
            List<DetalheAmigoViewModel> detalheAmigosViewModel = new List<DetalheAmigoViewModel>();

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(usuarioId);

            return usuario.Amigos.Select(amigo => new DetalheAmigoViewModel(amigo)).ToList();
        }

        public async Task<IEnumerable<DetalheJogoViewModel>> ObterTodosJogos(Guid usuarioId)
        {
            List<DetalheJogoViewModel> detalheJogosViewModel = new List<DetalheJogoViewModel>();

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(usuarioId);

            return usuario.Jogos.Select(jogo => new DetalheJogoViewModel(jogo)).ToList();
        }
    }
}
