﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;

namespace InvilliaIO.Application.Commands
{
    public class AdicionarJogoCommand : BaseCommand
    {
        public string Nome { get; private set; }

        public AdicionarJogoCommand(string nome)
        {
            Nome = nome;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new AdicionarJogoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class AdicionarJogoValidation : AbstractValidator<AdicionarJogoCommand>
    {
        public AdicionarJogoValidation()
        {
            RuleFor(c => c.Nome)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}