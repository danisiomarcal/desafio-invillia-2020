﻿using System;
using FluentValidation;
using InvilliaIO.Core.Mensagens;

namespace InvilliaIO.Application.Commands
{
    public class ExcluirUsuarioCommand: BaseCommand
    {
        public Guid UsuarioId { get; private set; }

        public ExcluirUsuarioCommand(
            Guid usuarioId)
        {
            UsuarioId = usuarioId;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new ExcluirUsuarioValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class ExcluirUsuarioValidation : AbstractValidator<ExcluirUsuarioCommand>
    {
        public ExcluirUsuarioValidation()
        {
            RuleFor(c => c.UsuarioId)
               .NotEmpty()
               .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}
