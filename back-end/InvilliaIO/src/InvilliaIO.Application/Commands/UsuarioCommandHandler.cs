﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using InvilliaIO.Application.Commands.Base;
using InvilliaIO.Application.Interfaces;
using InvilliaIO.Core.DomainObjetos;
using InvilliaIO.Core.Mediator;
using InvilliaIO.Core.Notificacoes;
using InvilliaIO.Domain;
using InvilliaIO.Domain.Interfaces;
using InvilliaIO.Domain.Modelos;
using MediatR;

namespace InvilliaIO.Application.Commands
{
    public class UsuarioCommandHandler :
           BaseCommandHandler,
           IRequestHandler<AdicionarUsuarioCommand, bool>,
           IRequestHandler<AlterarUsuarioCommand, bool>,
           IRequestHandler<ExcluirUsuarioCommand, bool>,
           IRequestHandler<AdicionarAmigoCommand, bool>,
           IRequestHandler<AlterarAmigoCommand, bool>,
           IRequestHandler<ExcluirAmigoCommand, bool>,
           IRequestHandler<AdicionarJogoCommand, bool>,
           IRequestHandler<AlterarJogoCommand, bool>,
           IRequestHandler<ExcluirJogoCommand, bool>,
           IRequestHandler<RealizarEmprestimoJogoCommand, bool>,
           IRequestHandler<RegistrarDevolucaoJogoCommand, bool>
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IUser _user;

        public UsuarioCommandHandler(IUsuarioRepository usuarioRepository,
                IUser user,
                IMediatorHandler mediatorHandler) :
            base(mediatorHandler)
        {
            _user = user;
            _usuarioRepository = usuarioRepository;
        }

        public async Task<bool> Handle(AdicionarUsuarioCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = new Usuario(
                request.Nome,
                request.Login,
                request.Senha,
                request.Ativo,
                (Usuario.TipoUsuario)request.Tipo
            );

            var usuarioExistente = await _usuarioRepository.Buscar(c => c.Login.ToUpper().Equals(usuario.Login.ToUpper()));

            if (usuarioExistente.Any())
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Já existe um usuário com este login."));
                return false;
            }

            await _usuarioRepository.Adicionar(usuario);

            return await _usuarioRepository.UnitOfWork.Commit();
        }

        public async Task<bool> Handle(AlterarUsuarioCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterPorId(request.Id);

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            usuario.Editar(
                request.Nome,
                request.Login,
                request.Senha,
                request.Ativo,
                (Usuario.TipoUsuario)request.Tipo
            );

            var usuarioExistente = await _usuarioRepository.Buscar(c => c.Login.ToUpper().Equals(usuario.Login.ToUpper()));

            if (usuarioExistente.Any() && usuarioExistente.First().Id != usuario.Id)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Já existe um usuário com este login."));
                return false;
            }

            _usuarioRepository.Atualizar(usuario);

            return await _usuarioRepository.UnitOfWork.Commit();
        }

        public async Task<bool> Handle(ExcluirUsuarioCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            if (request.UsuarioId == _user.GetId())
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Operação inválida. O usuário não pode excluir ele mesmo!"));
                return false;
            }

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(request.UsuarioId);

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            foreach (Jogo jogo in usuario.Jogos)
                _usuarioRepository.RemoverJogo(jogo);

            foreach (Amigo amigo in usuario.Amigos)
                _usuarioRepository.RemoverAmigo(amigo);

            _usuarioRepository.Remover(usuario);

            return await _usuarioRepository.UnitOfWork.Commit();
        }

        public async Task<bool> Handle(AdicionarAmigoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var amigo = new Amigo(request.Nome, usuario.Id);

            try
            {
                usuario.AdicionarAmigo(amigo);

                await _usuarioRepository.AdicionarAmigo(amigo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }

        public async Task<bool> Handle(AlterarAmigoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var amigo = await _usuarioRepository.ObterAmigoPorId(request.AmigoId);

            if (amigo == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Amigo inválido."));
                return false;
            }

            try
            {
                amigo.Editar(request.Nome);

                usuario.EditarAmigo(amigo);

                _usuarioRepository.AtualizarAmigo(amigo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }

        public async Task<bool> Handle(ExcluirAmigoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var amigo = await _usuarioRepository.ObterAmigoPorId(request.AmigoId);

            if (amigo == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Amigo inválido."));
                return false;
            }

            try
            {
                usuario.ExcluirAmigo(amigo);

                _usuarioRepository.RemoverAmigo(amigo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }

        public async Task<bool> Handle(AdicionarJogoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var jogo = new Jogo(request.Nome, usuario.Id);

            try
            {
                usuario.AdicionarJogo(jogo);

                await _usuarioRepository.AdicionarJogo(jogo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }

        public async Task<bool> Handle(AlterarJogoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var jogo = await _usuarioRepository.ObterJogoPorId(request.JogoId);

            if (jogo == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Jogo inválido."));
                return false;
            }

            try
            {
                jogo.Editar(request.Nome);

                usuario.EditarJogo(jogo);

                _usuarioRepository.AtualizarJogo(jogo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }

        public async Task<bool> Handle(ExcluirJogoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());

            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var jogo = await _usuarioRepository.ObterJogoPorId(request.JogoId);

            if (jogo == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Jogo inválido."));
                return false;
            }

            try
            {
                usuario.ExcluirJogo(jogo);

                _usuarioRepository.RemoverJogo(jogo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }

        public async Task<bool> Handle(RealizarEmprestimoJogoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());
            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var jogo = await _usuarioRepository.ObterJogoPorId(request.JogoId);
            if (jogo == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Jogo inválido."));
                return false;
            }

            var amigo = await _usuarioRepository.ObterAmigoPorId(request.AmigoId);
            if (amigo == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Amigo inválido."));
                return false;
            }

            try
            {
                usuario.EmprestarJogo(amigo, jogo);

                _usuarioRepository.AtualizarJogo(jogo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }

        public async Task<bool> Handle(RegistrarDevolucaoJogoCommand request, CancellationToken cancellationToken)
        {
            if (!ValidarComando(request))
                return false;

            var usuario = await _usuarioRepository.ObterUsuarioAmigosJogos(_user.GetId());
            if (usuario == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Usuário inválido."));
                return false;
            }

            var jogo = await _usuarioRepository.ObterJogoPorId(request.JogoId);
            if (jogo == null)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain("Jogo inválido."));
                return false;
            }

            try
            {
                usuario.RegistrarDevolucaoJogo(jogo);

                _usuarioRepository.AtualizarJogo(jogo);

                return await _usuarioRepository.UnitOfWork.Commit();
            }
            catch (DomainException ex)
            {
                await _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(ex.Mensagem));

                return false;
            }
        }
    }
}
