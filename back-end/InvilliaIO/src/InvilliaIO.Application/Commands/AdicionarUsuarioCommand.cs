﻿using System.Linq;
using FluentValidation;
using InvilliaIO.Core.Mensagens;
using InvilliaIO.Domain;

namespace InvilliaIO.Application.Commands
{
    public class AdicionarUsuarioCommand : BaseCommand
    {
        public string Nome { get; private set; }
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public char Tipo { get; private set; }
        public bool Ativo { get; private set; }

        public AdicionarUsuarioCommand(string nome,
            string login, string senha,
            char tipo,
            bool ativo)
        {
            Nome = nome;
            Login = login;
            Senha = senha;
            Tipo = tipo;
            Ativo = ativo;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new AdicionarUsuarioValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class AdicionarUsuarioValidation : AbstractValidator<AdicionarUsuarioCommand>
    {
        public AdicionarUsuarioValidation()
        {
            RuleFor(c => c.Nome)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");

            RuleFor(c => c.Login)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.")
                .Must(login => !login.Any(x => x == ' '))
                .WithMessage("O campo {PropertyName} não pode conter espaço em branco.");

            RuleFor(c => c.Senha)
                .NotEmpty()
                .WithMessage("O nome {PropertyName} é obrigatório.")
                .Must(senha => !senha.Any(x => x == ' '))
                .WithMessage("O campo {PropertyName} não pode conter espaço em branco.");

            RuleFor(c => c.Tipo)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.")
                .Must(tipo =>
                        (char)Usuario.TipoUsuario.Administrador == tipo ||
                        (char)Usuario.TipoUsuario.Normal == tipo)
                .WithMessage("O campo {PropertyName} deve ser do tipo (A)Administrador ou (N)Normal.");
        }
    }
}
