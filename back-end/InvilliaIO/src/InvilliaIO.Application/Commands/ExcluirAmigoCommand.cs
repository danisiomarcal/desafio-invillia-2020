﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;
using System;

namespace InvilliaIO.Application.Commands
{
    public class ExcluirAmigoCommand : BaseCommand
    {
        public Guid AmigoId { get; private set; }

        public ExcluirAmigoCommand(
            Guid amigoId)
        {
            AmigoId = amigoId;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new ExcluirAmigoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class ExcluirAmigoValidation : AbstractValidator<ExcluirAmigoCommand>
    {
        public ExcluirAmigoValidation()
        {
            RuleFor(c => c.AmigoId)
               .NotEmpty()
               .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}
