﻿using System;
using FluentValidation;
using InvilliaIO.Core.Mensagens;

namespace InvilliaIO.Application.Commands
{
    public class RealizarEmprestimoJogoCommand : BaseCommand
    {
        public Guid JogoId { get; private set; }
        public Guid AmigoId { get; private set; }

        public RealizarEmprestimoJogoCommand(Guid jogoId,
            Guid amigoId)
        {
            JogoId = jogoId;
            AmigoId = amigoId;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new RealizarEmprestimoJogoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class RealizarEmprestimoJogoValidation : AbstractValidator<RealizarEmprestimoJogoCommand>
    {
        public RealizarEmprestimoJogoValidation()
        {
            RuleFor(c => c.JogoId)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");

            RuleFor(c => c.AmigoId)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}
