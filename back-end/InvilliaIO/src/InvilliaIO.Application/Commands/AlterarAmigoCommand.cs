﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;
using System;

namespace InvilliaIO.Application.Commands
{
    public class AlterarAmigoCommand : BaseCommand
    {
        public string Nome { get; private set; }
        public Guid AmigoId { get; private set; }

        public AlterarAmigoCommand(string nome,
            Guid amigoId)
        {
            Nome = nome;
            AmigoId = amigoId;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new AlterarAmigoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class AlterarAmigoValidation : AbstractValidator<AlterarAmigoCommand>
    {
        public AlterarAmigoValidation()
        {
            RuleFor(c => c.AmigoId)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");

            RuleFor(c => c.Nome)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}
