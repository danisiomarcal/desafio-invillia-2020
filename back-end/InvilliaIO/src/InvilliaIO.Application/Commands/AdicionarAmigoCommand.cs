﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;

namespace InvilliaIO.Application.Commands
{
    public class AdicionarAmigoCommand: BaseCommand
    {
        public string Nome { get; private set; }

        public AdicionarAmigoCommand(string nome)
        {
            Nome = nome;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new AdicionarAmigoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class AdicionarAmigoValidation : AbstractValidator<AdicionarAmigoCommand>
    {
        public AdicionarAmigoValidation()
        {
            RuleFor(c => c.Nome)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}
