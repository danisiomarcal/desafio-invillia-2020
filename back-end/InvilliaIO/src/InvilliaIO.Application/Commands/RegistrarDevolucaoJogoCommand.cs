﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;
using System;

namespace InvilliaIO.Application.Commands
{
    public class RegistrarDevolucaoJogoCommand: BaseCommand
    {
        public Guid JogoId { get; private set; }

        public RegistrarDevolucaoJogoCommand(Guid jogoId)
        {
            JogoId = jogoId;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new RegistrarDevolucaoJogoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class RegistrarDevolucaoJogoValidation : AbstractValidator<RegistrarDevolucaoJogoCommand>
    {
        public RegistrarDevolucaoJogoValidation()
        {
            RuleFor(c => c.JogoId)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}
