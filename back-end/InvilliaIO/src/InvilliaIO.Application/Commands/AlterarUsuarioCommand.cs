﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;
using InvilliaIO.Domain;
using System;
using System.Linq;

namespace InvilliaIO.Application.Commands
{
    public class AlterarUsuarioCommand : BaseCommand
    {
        public Guid Id { get; private set; }
        public string Nome { get; private set; }
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public char Tipo { get; private set; }
        public bool Ativo { get; private set; }

        public AlterarUsuarioCommand(
            Guid id,
            string nome,
            string login, string senha,
            char tipo,
            bool ativo)
        {
            Id = id;
            Nome = nome;
            Login = login;
            Senha = senha;
            Tipo = tipo;
            Ativo = ativo;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new AlterarUsuarioValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class AlterarUsuarioValidation : AbstractValidator<AlterarUsuarioCommand>
    {
        public AlterarUsuarioValidation()
        {
            RuleFor(c => c.Id)
               .NotEmpty()
               .WithMessage("O campo {PropertyName} é obrigatório.");

            RuleFor(c => c.Nome)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");

            RuleFor(c => c.Login)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.")
                .Must(login => !login.Any(x => x == ' '))
                .WithMessage("O campo {PropertyName} não pode conter espaço em branco.");

            RuleFor(c => c.Senha)
                .NotEmpty()
                .WithMessage("O nome {PropertyName} é obrigatório.")
                .Must(senha => !senha.Any(x => x == ' '))
                .WithMessage("O campo {PropertyName} não pode conter espaço em branco.");

            RuleFor(c => c.Tipo)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.")
                .Must(tipo =>
                        (char)Usuario.TipoUsuario.Administrador == tipo ||
                        (char)Usuario.TipoUsuario.Normal == tipo)
                .WithMessage("O campo {PropertyName} deve ser do tipo (A)Administrador ou (N)Normal.");
        }
    }
}
