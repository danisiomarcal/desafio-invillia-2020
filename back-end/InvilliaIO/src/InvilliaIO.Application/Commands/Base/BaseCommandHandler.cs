﻿using InvilliaIO.Core.Mediator;
using InvilliaIO.Core.Mensagens;
using InvilliaIO.Core.Notificacoes;

namespace InvilliaIO.Application.Commands.Base
{
    public abstract class BaseCommandHandler
    {
        protected readonly IMediatorHandler _mediatorHandler;

        public BaseCommandHandler(IMediatorHandler mediatorHandle)
        {
            _mediatorHandler = mediatorHandle;
        }

        protected bool ValidarComando(BaseCommand command)
        {
            if (command.OperacaoValida()) return true;

            foreach (var erro in command.ValidationResult.Errors)
            {
                _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(erro.ErrorMessage));
            }

            return false;
        }
    }
}
