﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;
using System;

namespace InvilliaIO.Application.Commands
{
    public class AlterarJogoCommand : BaseCommand
    {
        public string Nome { get; private set; }
        public Guid JogoId { get; private set; }

        public AlterarJogoCommand(string nome,
            Guid jogoId)
        {
            Nome = nome;
            JogoId = jogoId;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new AlterarJogoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class AlterarJogoValidation : AbstractValidator<AlterarJogoCommand>
    {
        public AlterarJogoValidation()
        {
            RuleFor(c => c.JogoId)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");

            RuleFor(c => c.Nome)
                .NotEmpty()
                .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}