﻿using FluentValidation;
using InvilliaIO.Core.Mensagens;
using System;

namespace InvilliaIO.Application.Commands
{
    public class ExcluirJogoCommand : BaseCommand
    {
        public Guid JogoId { get; private set; }

        public ExcluirJogoCommand(
            Guid jogoId)
        {
            JogoId = jogoId;
        }

        public override bool OperacaoValida()
        {
            ValidationResult = new ExcluirJogoValidation().Validate(this);

            return ValidationResult.IsValid;
        }
    }

    internal class ExcluirJogoValidation : AbstractValidator<ExcluirJogoCommand>
    {
        public ExcluirJogoValidation()
        {
            RuleFor(c => c.JogoId)
               .NotEmpty()
               .WithMessage("O campo {PropertyName} é obrigatório.");
        }
    }
}
