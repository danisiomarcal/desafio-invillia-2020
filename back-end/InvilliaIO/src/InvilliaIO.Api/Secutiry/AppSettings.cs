﻿namespace InvilliaIO.Api.Secutiry
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public int ExpiracaoHoras { get; set; }
        public string AllowOrigin { get; set; }
    }
}
