﻿using InvilliaIO.Api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Net.Mime;

namespace InvilliaIO.Api.Setup
{
    public static class ModelStateInvalidResponse
    {
        public static IServiceCollection RegisterModelStateInvalidResponse(this IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson()
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.InvalidModelStateResponseFactory = context =>
                    {
                        var errosModel = context.ModelState.Where(m => m.Value.Errors.Count() > 0).Select(m => m.Value.Errors.FirstOrDefault().ErrorMessage);

                        var result = new BadRequestObjectResult(new CustomResponseViewModel { success = false, data = errosModel });

                        result.ContentTypes.Add(MediaTypeNames.Application.Json);

                        return result;
                    };
                });

            return services;
        }
    }
}
