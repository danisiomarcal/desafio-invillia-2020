﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using InvilliaIO.Application.Commands;
using InvilliaIO.Core.Mediator;
using InvilliaIO.Core.Notificacoes;
using InvilliaIO.Data.Repository;
using InvilliaIO.Domain.Interfaces;
using MediatR;
using InvilliaIO.Application.Queries;
using InvilliaIO.Application.Interfaces;
using InvilliaIO.Api.Secutiry;

namespace InvilliaIO.Api.Setup
{
    public static class DependencyInjection
    {
        public static IServiceCollection RegisterDependencyInjection(this IServiceCollection services)
        {
            services.AddScoped<JogosDBContext, JogosDBContext>();

            services.AddScoped<IUser, AspNetUser>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<IUsuarioQueries, UsuarioQueries>();

            services.AddScoped<IRequestHandler<AdicionarUsuarioCommand, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<AlterarUsuarioCommand, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<ExcluirUsuarioCommand, bool>, UsuarioCommandHandler>();

            services.AddScoped<IRequestHandler<AdicionarAmigoCommand, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<AlterarAmigoCommand, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<ExcluirAmigoCommand, bool>, UsuarioCommandHandler>();

            services.AddScoped<IRequestHandler<AdicionarJogoCommand, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<AlterarJogoCommand, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<ExcluirJogoCommand, bool>, UsuarioCommandHandler>();

            services.AddScoped<IRequestHandler<RealizarEmprestimoJogoCommand, bool>, UsuarioCommandHandler>();
            services.AddScoped<IRequestHandler<RegistrarDevolucaoJogoCommand, bool>, UsuarioCommandHandler>();

            services.AddScoped<INotificationHandler<NotificacaoDomain>, NotificacaoDomainHandler>();
            services.AddScoped<IMediatorHandler, MediatorHandler>();

            return services;
        }
    }
}
