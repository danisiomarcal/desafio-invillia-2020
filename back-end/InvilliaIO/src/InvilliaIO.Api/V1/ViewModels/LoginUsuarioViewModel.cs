﻿using System.ComponentModel.DataAnnotations;

namespace InvilliaIO.Api.V1.ViewModels
{
    public class LoginUsuarioViewModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        [RegularExpression(@"^\S+$", ErrorMessage = "O campo {0} não pode conter espaços.")]
        public string Login { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "O campo {0} é obrigatório.")]
        [RegularExpression(@"^\S+$", ErrorMessage = "O campo {0} não pode conter espaços.")]
        public string Senha { get; set; }
    }
}
