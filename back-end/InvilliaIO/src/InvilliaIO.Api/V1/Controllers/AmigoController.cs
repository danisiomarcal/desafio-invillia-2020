﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using MediatR;
using InvilliaIO.Api.Controllers;
using InvilliaIO.Core.Mediator;
using InvilliaIO.Api.Secutiry;
using InvilliaIO.Core.Notificacoes;
using InvilliaIO.Application.ViewModels.Request;
using InvilliaIO.Application.Commands;
using InvilliaIO.Application.Interfaces;
using System;
using InvilliaIO.Domain.Interfaces;
using InvilliaIO.Domain;
using System.Collections.Generic;
using InvilliaIO.Application.ViewModels.Response;
using InvilliaIO.Application.Queries;

namespace InvilliaIO.Api.V1.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}")]
    public class AmigoController : MainController
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IUser _user;
        private readonly IUsuarioQueries _usuarioQueries;

        public AmigoController(INotificationHandler<NotificacaoDomain> notifications,
                                 IMediatorHandler mediatorHandler,
                                 IUsuarioRepository usuarioRepository,
                                 IUser user,
                                 IUsuarioQueries usuarioQueries) :
            base(notifications, mediatorHandler)
        {
            _usuarioRepository = usuarioRepository;
            _user = user;
            _usuarioQueries = usuarioQueries;
        }

        [ClaimsAuthorize(Modulo.Amigo, PermissaoModulo.Gerenciar)]
        [HttpPost("amigos/novo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CadastroAmigoViewModel>> Cadastrar(CadastroAmigoViewModel cadastroAmigo)
        {
            var command = new AdicionarAmigoCommand(
                    cadastroAmigo.Nome
            );

            await EnviarCommand(command);

            return CustomResponse(cadastroAmigo);
        }

        [ClaimsAuthorize(Modulo.Amigo, PermissaoModulo.Gerenciar)]
        [HttpPut("amigos/{amigoId:}/alterar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CadastroAmigoViewModel>> Alterar([FromRoute] Guid amigoId, CadastroAmigoViewModel cadastroAmigo)
        {
            var amigo = await _usuarioRepository.ObterAmigoPorId(amigoId);

            if (amigo == null)
                return NotFound();

            var command = new AlterarAmigoCommand(
                    cadastroAmigo.Nome,
                    amigoId
            );

            await EnviarCommand(command);

            return CustomResponse(cadastroAmigo);
        }

        [ClaimsAuthorize(Modulo.Amigo, PermissaoModulo.Gerenciar)]
        [HttpDelete("amigos/{amigoId:Guid}/excluir")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Excluir([FromRoute] Guid amigoId)
        {
            var amigo = await _usuarioRepository.ObterAmigoPorId(amigoId);

            if (amigo == null)
                return NotFound();

            var command = new ExcluirAmigoCommand(
                amigoId
            );

            await EnviarCommand(command);

            return CustomResponse();
        }

        [ClaimsAuthorize(Modulo.Amigo, PermissaoModulo.Gerenciar)]
        [HttpGet("amigos/{amigoId:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<DetalheAmigoViewModel>> Detalhar([FromRoute] Guid amigoId)
        {
            var amigo = await _usuarioRepository.ObterAmigoPorId(amigoId);

            if (amigo == null || _user.GetId() != amigo.UsuarioId)
                return NotFound();

            return CustomResponse(new DetalheAmigoViewModel(amigo));
        }

        [ClaimsAuthorize(Modulo.Amigo, PermissaoModulo.Gerenciar)]
        [HttpGet("amigos/listar")]
        public async Task<ActionResult<IEnumerable<DetalheUsuarioViewModel>>> ObterTodosAmigos()
        {
            var todosAmigos = await _usuarioQueries.ObterTodosAmigos(_user.GetId());

            return CustomResponse(todosAmigos);
        }
    }
}