﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using InvilliaIO.Api.Controllers;
using InvilliaIO.Api.Secutiry;
using InvilliaIO.Application.Commands;
using InvilliaIO.Application.Interfaces;
using InvilliaIO.Application.Queries;
using InvilliaIO.Application.ViewModels.Request;
using InvilliaIO.Application.ViewModels.Response;
using InvilliaIO.Core.Mediator;
using InvilliaIO.Core.Notificacoes;
using InvilliaIO.Domain;
using InvilliaIO.Domain.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InvilliaIO.Api.V1.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}")]
    public class JogoController : MainController
    {
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IUser _user;
        private readonly IUsuarioQueries _usuarioQueries;

        public JogoController(INotificationHandler<NotificacaoDomain> notifications,
                                 IMediatorHandler mediatorHandler,
                                 IUsuarioRepository usuarioRepository,
                                 IUser user,
                                 IUsuarioQueries usuarioQueries) :
            base(notifications, mediatorHandler)
        {
            _usuarioRepository = usuarioRepository;
            _user = user;
            _usuarioQueries = usuarioQueries;
        }

        [ClaimsAuthorize(Modulo.Jogo, PermissaoModulo.Gerenciar)]
        [HttpPost("jogos/novo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CadastroJogoViewModel>> Cadastrar(CadastroJogoViewModel cadastroJogo)
        {
            var command = new AdicionarJogoCommand(
                    cadastroJogo.Nome
            );

            await EnviarCommand(command);

            return CustomResponse(cadastroJogo);
        }

        [ClaimsAuthorize(Modulo.Jogo, PermissaoModulo.Gerenciar)]
        [HttpPut("jogos/{jogoId:}/alterar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CadastroJogoViewModel>> Alterar([FromRoute] Guid jogoId, CadastroJogoViewModel cadastroJogo)
        {
            var jogo = await _usuarioRepository.ObterJogoPorId(jogoId);

            if (jogo == null)
                return NotFound();

            var command = new AlterarJogoCommand(
                    cadastroJogo.Nome,
                    jogoId
            );

            await EnviarCommand(command);

            return CustomResponse(cadastroJogo);
        }

        [ClaimsAuthorize(Modulo.Jogo, PermissaoModulo.Gerenciar)]
        [HttpDelete("jogos/{jogoId:Guid}/excluir")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Excluir([FromRoute] Guid jogoId)
        {
            var jogo = await _usuarioRepository.ObterJogoPorId(jogoId);

            if (jogo == null)
                return NotFound();

            var command = new ExcluirJogoCommand(
                jogoId
            );

            await EnviarCommand(command);

            return CustomResponse();
        }

        [ClaimsAuthorize(Modulo.Jogo, PermissaoModulo.Gerenciar)]
        [HttpGet("jogos/{jogoId:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<DetalheJogoViewModel>> Detalhar([FromRoute] Guid jogoId)
        {
            var jogo = await _usuarioRepository.ObterJogoPorId(jogoId);

            if (jogo == null || _user.GetId() != jogo.UsuarioId)
                return NotFound();

            return CustomResponse(new DetalheJogoViewModel(jogo));
        }

        [ClaimsAuthorize(Modulo.Jogo, PermissaoModulo.Gerenciar)]
        [HttpGet("jogos/listar")]
        public async Task<ActionResult<IEnumerable<DetalheJogoViewModel>>> ObterTodosJogos()
        {
            var todosJogos = await _usuarioQueries.ObterTodosJogos(_user.GetId());

            return CustomResponse(todosJogos);
        }

        [ClaimsAuthorize(Modulo.Jogo, PermissaoModulo.Gerenciar)]
        [HttpPost("jogos/{jogoId:Guid}/registrar-emprestimo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> RegistrarEmprestimo([FromRoute] Guid jogoId, EmprestimoJogoViewModel emprestimoJogo)
        {
            var jogo = await _usuarioRepository.ObterJogoPorId(jogoId);

            var amigo = await _usuarioRepository.ObterAmigoPorId(emprestimoJogo.AmigoId);

            if (jogo == null || _user.GetId() != jogo.UsuarioId || amigo == null || _user.GetId() != amigo.UsuarioId)
                return NotFound();

            var command = new RealizarEmprestimoJogoCommand(
                    jogoId,
                    emprestimoJogo.AmigoId
            );

            await EnviarCommand(command);

            return CustomResponse();
        }

        [ClaimsAuthorize(Modulo.Jogo, PermissaoModulo.Gerenciar)]
        [HttpPut("jogos/{jogoId:Guid}/registrar-devolucao")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> RegistrarDevolucao([FromRoute] Guid jogoId)
        {
            var jogo = await _usuarioRepository.ObterJogoPorId(jogoId);

            if (jogo == null || _user.GetId() != jogo.UsuarioId)
                return NotFound();

            var command = new RegistrarDevolucaoJogoCommand(
                    jogoId
            );

            await EnviarCommand(command);

            return CustomResponse();
        }
    }
}
