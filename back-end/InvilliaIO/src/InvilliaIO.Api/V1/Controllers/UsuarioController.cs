﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using MediatR;
using InvilliaIO.Api.Controllers;
using InvilliaIO.Core.Mediator;
using InvilliaIO.Api.Secutiry;
using InvilliaIO.Domain.Interfaces;
using InvilliaIO.Core.Notificacoes;
using InvilliaIO.Api.V1.ViewModels;
using InvilliaIO.Application.ViewModels.Request;
using InvilliaIO.Application.Commands;
using InvilliaIO.Application.ViewModels.Response;
using System.Collections.Generic;
using InvilliaIO.Application.Queries;
using InvilliaIO.Domain;

namespace InvilliaIO.Api.V1.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}")]
    public class UsuarioController : MainController
    {
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IUsuarioQueries _usuarioQueries;

        public UsuarioController(INotificationHandler<NotificacaoDomain> notifications,
                                 IMediatorHandler mediatorHandler,
                                 IUsuarioRepository usuarioRepository,
                                 IUsuarioQueries usuarioQueries,
                                 IOptions<AppSettings> appSettings) :
            base(notifications, mediatorHandler)
        {
            _usuarioRepository = usuarioRepository;
            _usuarioQueries = usuarioQueries;
            _appSettings = appSettings;
        }

        [ClaimsAuthorize(Modulo.Usuario, PermissaoModulo.Gerenciar)]
        [HttpGet("usuarios/{usuarioId:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<DetalheUsuarioViewModel>> Detalhar([FromRoute] Guid usuarioId)
        {
            var usuario = await _usuarioRepository.ObterPorId(usuarioId);

            if (usuario == null)
                return NotFound();

            return CustomResponse(new DetalheUsuarioViewModel(usuario));
        }

        [ClaimsAuthorize(Modulo.Usuario, PermissaoModulo.Gerenciar)]
        [HttpPost("usuarios/novo")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CadastroUsuarioViewModel>> Cadastrar(CadastroUsuarioViewModel cadastroUsuario)
        {
            var command = new AdicionarUsuarioCommand(
                    cadastroUsuario.Nome,
                    cadastroUsuario.Login,
                    cadastroUsuario.Senha,
                    Convert.ToChar(cadastroUsuario.Tipo),
                    cadastroUsuario.Ativo
            );

            await EnviarCommand(command);

            return CustomResponse(cadastroUsuario);
        }

        [ClaimsAuthorize(Modulo.Usuario, PermissaoModulo.Gerenciar)]
        [HttpPut("usuarios/{usuarioId:Guid}/alterar")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CadastroUsuarioViewModel>> Alterar([FromRoute] Guid usuarioId, CadastroUsuarioViewModel cadastroUsuario)
        {
            var usuario = await _usuarioRepository.ObterPorId(usuarioId);

            if (usuario == null)
                return NotFound();

            var command = new AlterarUsuarioCommand(
                usuarioId,
                cadastroUsuario.Nome,
                cadastroUsuario.Login,
                cadastroUsuario.Senha,
                Convert.ToChar(cadastroUsuario.Tipo),
                cadastroUsuario.Ativo
            );

            await EnviarCommand(command);

            return CustomResponse(cadastroUsuario);
        }

        [ClaimsAuthorize(Modulo.Usuario, PermissaoModulo.Gerenciar)]
        [HttpDelete("usuarios/{usuarioId:Guid}/excluir")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Excluir([FromRoute] Guid usuarioId)
        {
            var usuario = await _usuarioRepository.ObterPorId(usuarioId);

            if (usuario == null)
                return NotFound();

            var command = new ExcluirUsuarioCommand(
                usuarioId
            );

            await EnviarCommand(command);

            return CustomResponse();
        }

        [ClaimsAuthorize(Modulo.Usuario, PermissaoModulo.Gerenciar)]
        [HttpGet("usuarios/listar")]
        public async Task<ActionResult<IEnumerable<DetalheUsuarioViewModel>>> ObterTodosUsarios()
        {
            var todosUsuarios = await _usuarioQueries.ObterTodosUsuarios();

            return CustomResponse(todosUsuarios);
        }

        [AllowAnonymous]
        [HttpPost("usuarios/autenticar")]
        public async Task<ActionResult> Autenticar(LoginUsuarioViewModel loginUsuarioViewModel)
        {
            var usuario = (await _usuarioRepository.Buscar(p => p.Login.ToUpper().Equals(loginUsuarioViewModel.Login.ToUpper()) &&
                p.Senha.ToUpper().Equals(loginUsuarioViewModel.Senha.ToUpper()) && p.Ativo)).FirstOrDefault();

            if (usuario == null)
            {
                NotificarErro("Usuário inválido.");

                return CustomResponse(loginUsuarioViewModel);
            }

            return CustomResponse(new
            {
                token = TokenService.GenerateToken(usuario, _appSettings.Value),
                userToken = new
                {
                    nome = usuario.Nome,
                    claims = usuario.Permissoes.Select(p => new
                    {
                        tipo = p.Modulo,
                        valor = p.Operacao
                    })
                }
            });
        }
    }
}