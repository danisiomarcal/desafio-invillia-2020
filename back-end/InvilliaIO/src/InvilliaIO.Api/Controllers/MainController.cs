﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using InvilliaIO.Api.ViewModels;
using InvilliaIO.Core.Mediator;
using InvilliaIO.Core.Mensagens;
using InvilliaIO.Core.Notificacoes;
using MediatR;

namespace InvilliaIO.Api.Controllers
{
    [ApiController]
    [Authorize]
    public abstract class MainController : ControllerBase
    {
        private readonly NotificacaoDomainHandler _notifications;
        private readonly IMediatorHandler _mediatorHandler;

        public MainController(INotificationHandler<NotificacaoDomain> notifications,
                                 IMediatorHandler mediatorHandler)
        {
            _notifications = (NotificacaoDomainHandler)notifications;
            _mediatorHandler = mediatorHandler;
        }

        protected bool OperacaoValida()
        {
            return !_notifications.TemNotificacao();
        }

        protected void NotificarErro(string mensagem)
        {
            _mediatorHandler.PublicarNotificacao(new NotificacaoDomain(mensagem));
        }

        protected async Task<bool> EnviarCommand(BaseCommand command)
        {
            return await _mediatorHandler.EnviarCommand(command);
        }

        protected ActionResult CustomResponse(object result = null)
        {
            if (OperacaoValida())
            {
                return Ok(new CustomResponseViewModel
                {
                    success = true,
                    data = result
                });
            }

            return BadRequest(new CustomResponseViewModel
            {
                success = false,
                data = _notifications.ObterNotificacoes().Select(c => c.Mensagem)
            });
        }
    }
}
