﻿namespace InvilliaIO.Api.ViewModels
{
    public class CustomResponseViewModel
    {
        public bool success { get; set; }
        public object data { get; set; }
    }
}
