﻿using InvilliaIO.Domain.Interfaces.Base;
using InvilliaIO.Domain.Modelos;
using System;
using System.Threading.Tasks;

namespace InvilliaIO.Domain.Interfaces
{
    public interface IUsuarioRepository : IBaseRepository<Usuario>
    {
        Task<Amigo> ObterAmigoPorId(Guid amigoId);
        Task AdicionarAmigo(Amigo amigo);
        void AtualizarAmigo(Amigo amigo);
        void RemoverAmigo(Amigo amigo);
        Task<Jogo> ObterJogoPorId(Guid jogoId);
        Task AdicionarJogo(Jogo jogo);
        void AtualizarJogo(Jogo jogo);
        void RemoverJogo(Jogo jogo);
        Task<Usuario> ObterUsuarioAmigosJogos(Guid usuarioId);
    }
}
