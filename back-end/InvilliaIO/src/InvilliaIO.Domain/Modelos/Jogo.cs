﻿using InvilliaIO.Core.DomainObjetos;
using InvilliaIO.Core.DomainObjetos.Base;
using System;

namespace InvilliaIO.Domain.Modelos
{
    public class Jogo : Entity
    {
        public Jogo(string nome, Guid usuarioId) :
            base()
        {
            Nome = nome;
            UsuarioId = usuarioId;

            if (string.IsNullOrWhiteSpace(Nome))
                AdicionarErroDominio("Nome", "O campo Nome é obrigatório.");

            if (UsuarioId == Guid.Empty)
                AdicionarErroDominio("Nome", "O campo UsuarioId é obrigatório.");
        }

        public string Nome { get; private set; }

        public Guid UsuarioId { get; private set; }
        //EF
        public Usuario Usuario { get; private set; }

        public Guid? AmigoEmprestimoId { get; private set; }
        //EF
        public Amigo AmigoEmprestimo { get; private set; }

        public void Editar(string nome)
        {
            if (string.IsNullOrWhiteSpace(Nome)) 
            {
                AdicionarErroDominio("Nome", "O campo Nome é obrigatório.");
            }

            Nome = nome;
        }

        internal void AssociarAmigo(Guid amigoId)
        {
            AmigoEmprestimoId = amigoId;
        }

        internal void DesassociarAmigo()
        {
            AmigoEmprestimoId = null;
        }

        public bool EstaEmprestado()
        {
            return AmigoEmprestimoId.HasValue;
        }
    }
}
