﻿namespace InvilliaIO.Domain
{
    public static class Modulo
    {
        public const string Usuario = "Usuario";
        public const string Amigo = "Amigo";
        public const string Jogo = "Jogo";
    }

    public static class PermissaoModulo
    {
        public const string Gerenciar = "Gerenciar";
    }
}
