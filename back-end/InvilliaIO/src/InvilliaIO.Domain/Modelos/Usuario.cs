﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;
using InvilliaIO.Core.DomainObjetos;
using InvilliaIO.Core.DomainObjetos.Base;
using InvilliaIO.Domain.Modelos;

namespace InvilliaIO.Domain
{
    public class Usuario : Entity, IAggregateRoot
    {
        public Usuario(
            string nome,
            string login,
            string senha,
            bool ativo,
            TipoUsuario tipo) : base()
        {
            if (Validar(nome, login, senha))
            {
                Nome = nome;
                Login = login;
                Senha = senha;
                Ativo = ativo;
                Tipo = tipo;
            }
        }

        public string Nome { get; private set; }
        public string Login { get; private set; }
        public string Senha { get; private set; }
        public bool Ativo { get; private set; }
        public TipoUsuario Tipo { get; private set; }
        public IReadOnlyCollection<PermissaoUsuario> Permissoes
        {
            get
            {
                List<PermissaoUsuario> permissoes = new List<PermissaoUsuario>();

                if (Tipo == TipoUsuario.Administrador)
                    permissoes.Add(new PermissaoUsuario { Modulo = Modulo.Usuario, Operacao = PermissaoModulo.Gerenciar });

                permissoes.Add(new PermissaoUsuario { Modulo = Modulo.Amigo, Operacao = PermissaoModulo.Gerenciar });
                permissoes.Add(new PermissaoUsuario { Modulo = Modulo.Jogo, Operacao = PermissaoModulo.Gerenciar });

                return permissoes;
            }
        }

        private readonly List<Amigo> _amigos = new List<Amigo>();
        public IReadOnlyCollection<Amigo> Amigos => _amigos;

        private readonly List<Jogo> _jogos = new List<Jogo>();
        public IReadOnlyCollection<Jogo> Jogos => _jogos;

        public void Editar(string nome,
            string login,
            string senha,
            bool ativo,
            TipoUsuario tipo)
        {
            if (Validar(nome, login, senha))
            {
                Nome = nome;
                Login = login;
                Senha = senha;
                Ativo = ativo;
                Tipo = tipo;
            }
        }

        public void AdicionarAmigo(Amigo amigo)
        {
            if (_amigos.Any(a => a.Nome.ToUpper().Equals(amigo.Nome.ToUpper())))
            {
                AdicionarErroDominio("Amigo", "Este amigo já se encontra em sua lista.");
                return;
            }

            _amigos.Add(amigo);
        }

        public void AdicionarJogo(Jogo jogo)
        {
            if (_jogos.Any(a => a.Nome.ToUpper().Equals(jogo.Nome.ToUpper())))
            {
                AdicionarErroDominio("Jogo", "Este jogo já se encontra em sua lista.");
                return;
            }

            _jogos.Add(jogo);
        }

        public void ExcluirAmigo(Amigo amigo)
        {
            if (amigo.UsuarioId != Id)
            {
                AdicionarErroDominio("Amigo", "O amigo não se encontra na lista do usuário!");
                return;
            }

            if (amigo.TemJogoEmprestado())
            {
                AdicionarErroDominio("Amigo", "O amigo não pode ser excluído, pois tem jogo(s) a devolver!");
                return;
            }

            _amigos.Remove(amigo);
        }

        public void EditarAmigo(Amigo amigo)
        {
            if (amigo.UsuarioId != Id)
            {
                AdicionarErroDominio("Amigo", "O amigo não se encontra na lista do usuário!");
                return;
            }

            if (_amigos.Any(a => a.Nome.ToUpper().Equals(amigo.Nome.ToUpper()) && a.Id != amigo.Id))
            {
                AdicionarErroDominio("Amigo", "Não é possível alterar o amigo, pois já existe um com o mesmo nome!");
                return;
            }

            _amigos.RemoveAt(_amigos.FindIndex(a => a.Id == amigo.Id));

            _amigos.Add(amigo);
        }

        public void ExcluirJogo(Jogo jogo)
        {
            if (jogo.UsuarioId != Id)
            {
                AdicionarErroDominio("Jogo", "O jogo não se encontra na lista do usuário!");
                return;
            }

            _jogos.Remove(jogo);
        }

        public void EditarJogo(Jogo jogo)
        {
            if (jogo.UsuarioId != Id)
            {
                AdicionarErroDominio("Jogo", "O jogo não se encontra na lista do usuário!");
                return;
            }

            if (_jogos.Any(j => j.Nome.ToUpper().Equals(jogo.Nome.ToUpper()) && j.Id != jogo.Id))
            {
                AdicionarErroDominio("Jogo", "Não é possível alterar o jogo, pois já existe um com o mesmo nome!");
                return;
            }

            _jogos.RemoveAt(_jogos.FindIndex(j => j.Id == jogo.Id));

            _jogos.Add(jogo);
        }

        public void EmprestarJogo(Amigo amigo, Jogo jogo)
        {
            if (amigo.UsuarioId != Id)
            {
                AdicionarErroDominio("Amigo", "O amigo não se encontra na lista do usuário!");
                return;
            }

            if (jogo.UsuarioId != Id)
            {
                AdicionarErroDominio("Jogo", "O jogo não se encontra na lista do usuário!");
                return;
            }

            if (jogo.EstaEmprestado())
            {
                AdicionarErroDominio("Jogo", "Este jogo já está emprestado!");
                return;
            }

            jogo.AssociarAmigo(amigo.Id);
        }

        public void RegistrarDevolucaoJogo(Jogo jogo)
        {
            if (jogo.UsuarioId != Id)
            {
                AdicionarErroDominio("Jogo", "O jogo não se encontra na lista do usuário!");
                return;
            }

            if (!jogo.EstaEmprestado())
            {
                AdicionarErroDominio("Jogo", "Este jogo não está emprestado!");
                return;
            }

            jogo.DesassociarAmigo();
        }

        private bool Validar(string nome, string login, string senha)
        {
            if (string.IsNullOrWhiteSpace(nome))
                AdicionarErroDominio("Nome", "O campo nome é obrigatório.");

            if (string.IsNullOrWhiteSpace(login))
                AdicionarErroDominio("Login", "O campo login é obrigatório.");

            if (string.IsNullOrWhiteSpace(senha))
                AdicionarErroDominio("Senha", "O campo senha é obrigatório.");

            return !TemErroDominio();
        }

        public enum TipoUsuario
        {
            Administrador = 'A',
            Normal = 'N'
        }
    }

    public class PermissaoUsuario
    {
        public string Modulo { get; set; }
        public string Operacao { get; set; }
    }
}
