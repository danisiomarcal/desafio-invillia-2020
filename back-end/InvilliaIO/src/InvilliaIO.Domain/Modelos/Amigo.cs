﻿using System;
using System.Collections.Generic;
using System.Linq;
using InvilliaIO.Core.DomainObjetos;
using InvilliaIO.Core.DomainObjetos.Base;

namespace InvilliaIO.Domain.Modelos
{
    public class Amigo : Entity
    {
        public Amigo(string nome, Guid usuarioId) :
            base()
        {
            Nome = nome;
            UsuarioId = usuarioId;
        }

        public string Nome { get; private set; }
        public Guid UsuarioId { get; private set; }

        //EF
        public Usuario Usuario { get; private set; }

        private readonly List<Jogo> _jogosEmprestados = new List<Jogo>();
        public IReadOnlyCollection<Jogo> JogosEmprestados => _jogosEmprestados;

        public void Editar(string nome)
        {
            Nome = nome;
        }

        public bool TemJogoEmprestado()
        {
            return _jogosEmprestados.Any();
        }

        public string Apelido { get; private set; }
    }
}
