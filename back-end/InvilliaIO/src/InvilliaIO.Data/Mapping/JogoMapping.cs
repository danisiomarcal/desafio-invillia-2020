﻿using InvilliaIO.Domain.Modelos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace InvilliaIO.Data.Mapping
{
    public class JogoMapping: IEntityTypeConfiguration<Jogo>
    {
        public void Configure(EntityTypeBuilder<Jogo> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("id");

            builder.Property(c => c.Nome)
                .HasColumnName("nome")
                .IsRequired();

            builder.Property(c => c.UsuarioId)
                .HasColumnName("usuario_id")
                .IsRequired();
            
            builder.Property(c => c.AmigoEmprestimoId)
                .HasColumnName("amigo_emprestimo_id");

            builder.ToTable("jogo");
        }
    }
}
