﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using InvilliaIO.Domain;

namespace InvilliaIO.Data.Mapping
{
    public class UsuarioMapping : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("id");

            builder.Property(c => c.Nome)
                .HasColumnName("nome")
                .IsRequired();

            builder.Property(c => c.Login)
                .HasColumnName("login")
                .IsRequired();

            builder.Property(c => c.Senha)
                .HasColumnName("senha")
                .IsRequired();

            builder.Property(c => c.Tipo)
                .HasConversion(
                    v => (char)v,
                    v => (Usuario.TipoUsuario)v)
                .HasColumnName("tipo")
                .IsRequired();

            builder.Property(c => c.Ativo)
                .HasColumnName("ativo")
                .IsRequired();

            builder.HasMany(s => s.Amigos)
                .WithOne(s => s.Usuario)
                .HasForeignKey(s => s.UsuarioId);

            builder.HasMany(s => s.Jogos)
                .WithOne(s => s.Usuario)
                .HasForeignKey(s => s.UsuarioId);

            builder.ToTable("usuario");
        }
    }
}
