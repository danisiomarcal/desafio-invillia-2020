﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using InvilliaIO.Domain.Modelos;

namespace InvilliaIO.Data.Mapping
{
    public class AmigoMapping : IEntityTypeConfiguration<Amigo>
    {
        public void Configure(EntityTypeBuilder<Amigo> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("id");

            builder.Property(c => c.Nome)
                .HasColumnName("nome")
                .IsRequired();

            builder.Property(c => c.UsuarioId)
                .HasColumnName("usuario_id")
                .IsRequired();

            builder.HasMany(s => s.JogosEmprestados)
                .WithOne(s => s.AmigoEmprestimo)
                .HasForeignKey(s => s.AmigoEmprestimoId);

            builder.Property(c => c.Apelido)
                .HasColumnName("apelido");

            builder.ToTable("amigo");
        }
    }
}
