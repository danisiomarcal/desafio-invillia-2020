﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace InvilliaIO.Data.Repository
{
    public class JogosDBContextFactory : IDesignTimeDbContextFactory<JogosDBContext>
    {
        public JogosDBContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<JogosDBContext>();
            optionsBuilder.UseSqlServer("Server=localhost;Database=invillia2io;User Id=sa;Password=system;MultipleActiveResultSets=true");

            return new JogosDBContext(optionsBuilder.Options);
        }
    }
}
