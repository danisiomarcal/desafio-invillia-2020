﻿using InvilliaIO.Data.Repository.Base;
using InvilliaIO.Domain;
using InvilliaIO.Domain.Interfaces;
using InvilliaIO.Domain.Modelos;
using System;
using System.Threading.Tasks;

namespace InvilliaIO.Data.Repository
{
    public class UsuarioRepository : BaseRepository<Usuario>, IUsuarioRepository
    {
        public UsuarioRepository(JogosDBContext db) : base(db)
        {
        }

        public async Task<Amigo> ObterAmigoPorId(Guid amigoId)
        {
            var amigo = await _dbContext.Amigos.FindAsync(amigoId);

            if (amigo != null)
            {
                await _dbContext.Entry(amigo)
                .Collection(a => a.JogosEmprestados)
                .LoadAsync();
            }

            return amigo;
        }

        public async Task AdicionarAmigo(Amigo amigo)
        {
            await _dbContext.Amigos.AddAsync(amigo);
        }

        public void AtualizarAmigo(Amigo amigo)
        {
            _dbContext.Amigos.Update(amigo);
        }

        public void RemoverAmigo(Amigo amigo)
        {
            _dbContext.Amigos.Remove(amigo);
        }

        public async Task<Jogo> ObterJogoPorId(Guid jogoId)
        {
            var jogo = await _dbContext.Jogos.FindAsync(jogoId);

            if (jogo != null)
            {
                await _dbContext.Entry(jogo)
                    .Reference(j => j.AmigoEmprestimo)
                    .LoadAsync();
            }

            return jogo;
        }

        public async Task AdicionarJogo(Jogo jogo)
        {
            await _dbContext.Jogos.AddAsync(jogo);
        }

        public void AtualizarJogo(Jogo jogo)
        {
            _dbContext.Jogos.Update(jogo);
        }

        public void RemoverJogo(Jogo jogo)
        {
            _dbContext.Jogos.Remove(jogo);
        }

        public async Task<Usuario> ObterUsuarioAmigosJogos(Guid usuarioId)
        {
            var usuario = await _dbSet.FindAsync(usuarioId);

            if (usuario != null)
            {
                await _dbContext.Entry(usuario)
                .Collection(u => u.Amigos)
                .LoadAsync();

                await _dbContext.Entry(usuario)
                    .Collection(u => u.Jogos)
                    .LoadAsync();
            }

            return usuario;
        }
    }
}
