﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using InvilliaIO.Core.Data;
using InvilliaIO.Domain;
using InvilliaIO.Domain.Modelos;
using FluentValidation.Results;

namespace InvilliaIO.Data.Repository
{
    public class JogosDBContext : DbContext, IUnitOfWork
    {
        public JogosDBContext(DbContextOptions<JogosDBContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(JogosDBContext).Assembly);

            modelBuilder.Ignore<PermissaoUsuario>();
            modelBuilder.Ignore<ValidationResult>();
        }

        public async Task<bool> Commit()
        {
            return await base.SaveChangesAsync() > 0;
        }

        public DbSet<Amigo> Amigos { get; set; }
        public DbSet<Jogo> Jogos { get; set; }
    }
}
