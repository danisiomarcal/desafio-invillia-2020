﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq.Expressions;
using System.Linq;
using InvilliaIO.Core.Data;
using InvilliaIO.Domain.Interfaces.Base;
using InvilliaIO.Core.DomainObjetos.Base;

namespace InvilliaIO.Data.Repository.Base
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IAggregateRoot
    {
        protected readonly JogosDBContext _dbContext;
        protected readonly DbSet<TEntity> _dbSet;

        public IUnitOfWork UnitOfWork => _dbContext;

        protected BaseRepository(JogosDBContext db)
        {
            _dbContext = db;

            _dbSet = db.Set<TEntity>();
        }

        public async Task<TEntity> ObterPorId(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }

        public async Task Adicionar(TEntity entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public void Atualizar(TEntity entity)
        {
            _dbSet.Update(entity);
        }

        public void Remover(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public virtual async Task<IEnumerable<TEntity>> ObterTodos()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> Buscar(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.AsNoTracking().Where(predicate).ToListAsync();
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
        }
    }
}
