﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InvilliaIO.Data.Migrations
{
    public partial class PrimeiraMigracao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "usuario",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    nome = table.Column<string>(nullable: false),
                    login = table.Column<string>(nullable: false),
                    senha = table.Column<string>(nullable: false),
                    ativo = table.Column<bool>(nullable: false),
                    tipo = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_usuario", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "amigo",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    nome = table.Column<string>(nullable: false),
                    usuario_id = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_amigo", x => x.id);
                    table.ForeignKey(
                        name: "FK_amigo_usuario_usuario_id",
                        column: x => x.usuario_id,
                        principalTable: "usuario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "jogo",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    nome = table.Column<string>(nullable: false),
                    usuario_id = table.Column<Guid>(nullable: false),
                    amigo_emprestimo_id = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_jogo", x => x.id);
                    table.ForeignKey(
                        name: "FK_jogo_amigo_amigo_emprestimo_id",
                        column: x => x.amigo_emprestimo_id,
                        principalTable: "amigo",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_jogo_usuario_usuario_id",
                        column: x => x.usuario_id,
                        principalTable: "usuario",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_amigo_usuario_id",
                table: "amigo",
                column: "usuario_id");

            migrationBuilder.CreateIndex(
                name: "IX_jogo_amigo_emprestimo_id",
                table: "jogo",
                column: "amigo_emprestimo_id");

            migrationBuilder.CreateIndex(
                name: "IX_jogo_usuario_id",
                table: "jogo",
                column: "usuario_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "jogo");

            migrationBuilder.DropTable(
                name: "amigo");

            migrationBuilder.DropTable(
                name: "usuario");
        }
    }
}
