﻿using FluentValidation.Results;
using System;
using System.Linq;

namespace InvilliaIO.Core.DomainObjetos.Base
{
    public abstract class Entity
    {
        public ValidationResult ValidationResult { get; protected set; }

        public Guid Id { get; private set; }

        protected Entity()
        {
            Id = Guid.NewGuid();
            ValidationResult = new ValidationResult();
        }

        protected void AdicionarErroDominio(string erro, string mensagem)
        {
            ValidationResult.Errors.Add(new ValidationFailure(erro, mensagem));
        }

        public bool TemErroDominio() 
        {
            return ValidationResult.Errors.Any();
        }
    }
}
