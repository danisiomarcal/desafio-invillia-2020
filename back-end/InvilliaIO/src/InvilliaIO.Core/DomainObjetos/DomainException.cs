﻿using System;

namespace InvilliaIO.Core.DomainObjetos
{
    public class DomainException : Exception
    {
        public readonly string Mensagem;

        public DomainException(string mensagem) : base(mensagem)
        {
            Mensagem = mensagem;
        }
    }
}
