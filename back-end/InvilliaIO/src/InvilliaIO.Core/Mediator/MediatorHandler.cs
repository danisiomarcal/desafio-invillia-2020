﻿using System.Threading.Tasks;
using MediatR;
using InvilliaIO.Core.Mensagens;
using InvilliaIO.Core.Notificacoes;

namespace InvilliaIO.Core.Mediator
{
    public class MediatorHandler : IMediatorHandler
    {
        private readonly IMediator _mediator;

        public MediatorHandler(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async Task<bool> EnviarCommand<T>(T command) where T : BaseCommand
        {
            return await _mediator.Send(command);
        }

        public async Task PublicarNotificacao(NotificacaoDomain notificacaoDomain)
        {
            await _mediator.Publish(notificacaoDomain);
        }
    }
}
