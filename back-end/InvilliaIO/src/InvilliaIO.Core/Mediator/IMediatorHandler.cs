﻿using InvilliaIO.Core.Mensagens;
using InvilliaIO.Core.Notificacoes;
using System.Threading.Tasks;

namespace InvilliaIO.Core.Mediator
{
    public interface IMediatorHandler
    {
        public Task<bool> EnviarCommand<T>(T command) where T : BaseCommand;
        public Task PublicarNotificacao(NotificacaoDomain notificacao);
    }
}
