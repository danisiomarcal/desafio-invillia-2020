﻿using FluentValidation.Results;
using MediatR;

namespace InvilliaIO.Core.Mensagens
{
    public abstract class BaseCommand : IRequest<bool>
    {
        public ValidationResult ValidationResult { get; protected set; }

        public abstract bool OperacaoValida();
    }
}
