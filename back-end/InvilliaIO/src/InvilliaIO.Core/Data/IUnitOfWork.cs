﻿using System.Threading.Tasks;

namespace InvilliaIO.Core.Data
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}
