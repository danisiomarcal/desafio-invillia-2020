﻿using MediatR;

namespace InvilliaIO.Core.Notificacoes
{
    public class NotificacaoDomain : INotification
    {
        public string Mensagem { get; private set; }

        public NotificacaoDomain(string mensagem)
        {
            Mensagem = mensagem;
        }
    }
}
