﻿using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace InvilliaIO.Core.Notificacoes
{
    public class NotificacaoDomainHandler : INotificationHandler<NotificacaoDomain>
    {
        private List<NotificacaoDomain> _notificacoes;

        public NotificacaoDomainHandler()
        {
            _notificacoes = new List<NotificacaoDomain>();
        }

        public Task Handle(NotificacaoDomain notificacoes, CancellationToken cancellationToken)
        {
            _notificacoes.Add(notificacoes);

            return Task.CompletedTask;
        }

        public virtual List<NotificacaoDomain> ObterNotificacoes()
        {
            return _notificacoes;
        }

        public virtual bool TemNotificacao()
        {
            return ObterNotificacoes().Any();
        }

        public void Dispose()
        {
            _notificacoes = new List<NotificacaoDomain>();
        }
    }
}
