CREATE DATABASE [invilliaio];
GO
USE [invilliaio];
GO

CREATE TABLE [usuario](
	[id] uniqueidentifier NOT NULL,
	[nome] varchar(50) NOT NULL,
	[login] varchar(20) NOT NULL,
	[senha] varchar(20) NOT NULL,
	[ativo] bit NOT NULL,
	[tipo] char(1) check (tipo in ('A','N')) NOT NULL,
	primary key([id]),
	constraint FK_LoginUsuario unique ([nome])
);

CREATE TABLE [amigo](
	[id] uniqueidentifier NOT NULL,
	[nome] varchar(50) NOT NULL,
	[usuario_id] uniqueidentifier NOT NULL,
	primary key([id]),
	constraint FK_UsuarioAmigo foreign key ([usuario_id])
	references [usuario](id),
	constraint FK_NomeAmigo unique ([usuario_id],[nome])
);

CREATE TABLE [jogo](
	[id] uniqueidentifier NOT NULL,
	[nome] varchar(50) NOT NULL,
	[usuario_id] uniqueidentifier NOT NULL,
	[amigo_emprestimo_id] uniqueidentifier NULL,
	primary key([id]),
	constraint FK_UsuarioJogo foreign key ([usuario_id])
	references [usuario](id),
	constraint FK_AmigoJogo foreign key ([amigo_emprestimo_id])
	references [amigo](id),
	constraint FK_Nomejogo unique ([usuario_id],[nome])
);

insert into [usuario] ([id],[nome],[login],[senha],[ativo],[tipo]) values ('b3ee0ca5-3905-4743-83b1-cf9cdc43107a','Administrador','admin','admin',1,'A');

